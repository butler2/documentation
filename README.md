# Butler Documentation

[[_TOC_]]

## Introduction

Hello! 

Butler is a custom Discord bot designed to exist in a small handful of servers and provide a small set of fun features for the users within those servers.

## Important Notes

1. ### Privacy and Safety
    Butler does not collect nor maintain any of the following:
    - User data
    - Conversation records
    - Interaction records
    - Server data 

    <br> All interactions with Butler are constrained to purely input:output mapping rules and no information is persistently stored. Any changes to this persistent storage policy will be reflected upon this page, and the bot will signal an alert to all active servers that a policy has been adjusted.

2. ### Source Code is Private
    Due to the nature of the bot and its hosting options, I feel it safest for myself and for every participating server if the sourcecode of Butler remains private. This removes any chance of someone utilizing knowledge of its infrastructure in order to do harm.

3. ### Feature Requests
    New features for Butler can be requested. Please use the the [Issue Feature](https://gitlab.com/butler2/documentation/-/issues) or contact me on Discord.

## Command Documentation

| Command | Required Arguments | Optional Arguments | Description | Example |
| ---      | ---      | ---      | --- | --- |
| !roll   | [N]D[N]   | adv, dis, +[N], -[N]   | Rolls a specified number of dice with a specified number of sides. Advantage, Disadvantage, or static numeric bonuses can be applied. | `!roll 1d20 adv +5`
| !hello | None | None | Responds with a greeting, tagging the user.
| !commands  |  None | None   | Posts a list of enabled commands, their usages, and a link to this documentation page. 